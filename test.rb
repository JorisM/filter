#!/usr/bin/env ruby
require 'json'
require 'net/http'
#DEVICE_MAC = "64:0:2d:1:ce:34" # COFFEE MACHINE
#DEVICE_IP = `arp -a | grep #{DEVICE_MAC}`[/\b(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\b/, 1]
DEVICE_IP = "192.168.87.26"

unless DEVICE_IP
  STDERR.puts("DEVICE #{DEVICE_MAC} NOT FOUND ON NETWORK")
  exit
end

ENDPOINT_URI = URI("http://#{DEVICE_IP}/report")

loop do
  report = JSON.parse(Net::HTTP.get(ENDPOINT_URI))
  puts report["power"]
  STDOUT.flush
  sleep 1
end
