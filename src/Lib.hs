{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards       #-}

module Lib
    ( checkForCoffee
    ) where



import qualified Control.Foldl              as Foldl
import           Control.Monad
import           Control.Monad.Trans.Reader (ReaderT (runReaderT))
import           Data.Aeson
import           Data.Aeson.Types           (Parser)
import           Data.ByteString            hiding (pack)
import           Data.ByteString.Char8      (pack, unpack)
import qualified Data.ByteString.Lazy.Char8 as Lazy
import qualified Data.CaseInsensitive       as CI
import           Data.Either                (Either (..))
import           Data.List
import           Data.Maybe                 as Maybe
import           Data.Semigroup             ((<>))
import           Data.Text                  hiding (pack)
import           Debug.Trace
import           GHC.Generics
import qualified Network.WebSockets         as WS
import           Options.Applicative
import           Pipes
import           Pipes.Concurrent
import qualified Pipes.Prelude              as Pipes
import qualified Pipes.Prelude              as P
import           Pipes.WebSockets
import           Text.Read

  -- {
  --   "event":"report",
  --   "time":"2017-03-16T12:42:26Z",
  --   "data":{
  --     "relay":true,
  --     "power":0.000000
  --   }
  -- }
data Report = Report {
  reportEvent :: String,
  reportTime  :: String,
  reportRelay :: Bool,
  reportPower :: Float } deriving Show

instance FromJSON Report where
  parseJSON = withObject "data" $ \o -> do
    reportEvent <- o .: "event"
    reportTime <- o .: "time"
    dataField <- o .: "data"
    reportRelay <- dataField .: "relay"
    reportPower <- dataField .: "power"
    return Report{..}

traceThis :: Either String Report -> Maybe Report
traceThis x =
  case x of
    Left error ->
      Debug.Trace.trace ("error parsing: " ++ error) Nothing
    Right report ->
      Just report

toLazyStr =
  Lazy.pack . Data.ByteString.Char8.unpack

toFloat =
  Maybe.fromMaybe 0.0
  . Text.Read.readMaybe
  . Data.ByteString.Char8.unpack


data State =
  Idle | Lungo | Espresso | Watching deriving Show

foldlSummary :: Foldl.Fold ByteString (Int, [State])
foldlSummary = Foldl.Fold step begin done
  where
       begin = (Idle,(0, []))
       step acc toParse =
                  let
                    (watchingCounter_, acc_) = snd acc
                    last34 = Data.List.take 34 $ acc_
                    floatsAcc = fmap toFloat last34
                    val = Data.List.sum $ fmap (\x -> if x > 200 then 1 else 0) $ floatsAcc
                    decoded =  Maybe.fromMaybe (pack "0.0")
                                $ fmap (pack . show . reportPower)
                                $ traceThis
                                $ eitherDecode
                                $ toLazyStr toParse
                    watchingCounter = if val > 0 then watchingCounter_ + 1 else 0
                  in
                    case val of
                      v | v >= 15 && watchingCounter > 32 ->
                        (Lungo, (0, []))
                      v | v >= 8 && watchingCounter > 32 ->
                        (Espresso, (0, []))
                      v | v >= 1 ->
                        (Watching, (watchingCounter, decoded : last34))
                      _ ->
                        (Idle, (0, decoded : last34))
       done (acc, res) = (0, [acc])

showStream :: Foldl.Fold ByteString (Int, [State]) -> Producer ByteString IO () -> IO ()
showStream f p =
  runEffect $
    p >-> Foldl.purely Pipes.scan f >->
    Pipes.chain (\x -> Prelude.putStrLn $ show $ Maybe.fromMaybe Idle (Maybe.listToMaybe $ snd x)) >->
    forever await


--   > main :: IO ()
--   > main = filterCoffeeWebsocket "127.0.0.1" 9000 "/"
filterCoffeeWebsocket :: String  -- ^ Server to connect to
             -> Int     -- ^ Port
             -> String  -- ^ Path from the server, i.e. "/".
             -> String  -- ^ The subprotocol, i.e. "mystrom-report-protocol"
             -> IO ()
filterCoffeeWebsocket host port path subprot = do
    -- Setup a "mailbox" for getting messages from and placing them in.
    (output, input) <- spawn unbounded
    let
        -- Take messages from the socket and put them in the output.
        toInboxFromServer = runEffect $ wsIn >-> toOutput output
        --  Read messages from the socket (which have been placed on the mailbox) and print to stdout.
        workThatInbox = showStream foldlSummary (fromInput input)
    lvf $ workThatInbox

    WS.runClientWith
      host
      port
      path
      WS.defaultConnectionOptions
      [(CI.mk $ pack "Sec-WebSocket-Protocol", pack subprot)]
      (runReaderT toInboxFromServer)
    where
        lvf = liftIO . void . forkIO


data Config = Config
  { host     :: String
  , port     :: Int
  , protocol :: String }

commandlineParams :: Options.Applicative.Parser Config
commandlineParams = Config
      <$> strOption
          ( long "host"
         <> metavar "Host"
         <> help "i.e. 127.0.0.1" )
      <*> option auto
          ( long "port"
         <> help "port"
         <> showDefault
         <> value 9000
         <> metavar "INT" )
      <*> strOption
          ( long "protocol"
         <> metavar "protocol"
         <> help "i.e. mystrom-report-protocol" )


checkForCoffee :: IO ()
checkForCoffee =
  runFilter =<< execParser opts
  where
    opts = info (commandlineParams <**> helper)
      ( fullDesc
     <> progDesc "Filter coffees"
     <> header "welcome welcome... call me maybe? jorismorger@gmail.com" )

runFilter :: Config -> IO ()
runFilter (Config h p s) = filterCoffeeWebsocket h p "/" s
runFilter _              = return ()

